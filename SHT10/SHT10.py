def temp_hum():
    from sht1x.Sht1x import Sht1x as SHT1x
    import RPi.GPIO as GPIO

    GPIO.setmode(GPIO.BOARD)

    dataPin = 7
    cskPin = 11
    mysht1x = SHT1x(dataPin, cskPin, SHT1x.GPIO_BOARD)

    temperature = mysht1x.read_temperature_C()
    humidity = mysht1x.read_humidity()
    dewPoint = mysht1x.calculate_dew_point(temperature, humidity)

    print("Temperature: {} Humidity: {} Dew Point: {}".format(temperature, humidity, dewPoint))
    #print("Temperatura: {} Humedad: {}" .format(temperature, humidity))
    return temperature, humidity, dewPoint
